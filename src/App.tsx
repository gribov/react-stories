import * as React from 'react';

import StoriesCarousel from 'src/components/stories/carousel/StoriesCarousel';
import { IStoriesCarouselItem } from 'src/components/stories/carousel/StoriesCarouselItem';

const allItems: IStoriesCarouselItem[] = [
  { key: 1, src: 'img/1.jpg', title: 'First', url: 'img/1.jpg' },
  { key: 2, src: 'img/2.jpg', title: 'Second', url: 'img/1.jpg' },
  { key: 3, src: 'img/3.jpg', title: 'Third', url: 'img/1.jpg' },
  { key: 4, src: 'img/1.jpg', title: 'First', url: 'img/1.jpg' },
  { key: 5, src: 'img/2.jpg', title: 'Second', url: 'img/1.jpg' },
  { key: 6, src: 'img/3.jpg', title: 'Third', url: 'img/1.jpg' },
  { key: 7, src: 'img/1.jpg', title: 'Third', url: 'img/1.jpg' },
  { key: 8, src: 'img/2.jpg', title: 'Third', url: 'img/1.jpg' },
  { key: 9, src: 'img/3.jpg', title: 'Third', url: 'img/1.jpg' },
  { key: 10, src: 'img/1.jpg', title: 'Third', url: 'img/1.jpg' },
];

class App extends React.Component<{}, {}> {
  public state = {
    items: allItems.slice(0, 3),
  };

  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>React Carousel</h1>
          <button onClick={this.addItems}>ADD</button>
        </header>
        <div style={{ marginTop: '30px' }}>
          <StoriesCarousel items={this.state.items} />
        </div>
      </div>
    );
  }
  addItems = e => {
    e.preventDefault();
    // add reinit !
    const items = allItems.slice(this.state.items.length, this.state.items.length + 3);

    this.setState({
      items: this.state.items.concat(items),
    });
  };
}

export default App;
