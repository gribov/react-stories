import * as React from 'react';

import CarouselItem from 'src/components/carousel/Item';

export interface IStoriesCarouselItem {
  key: string | number;
  src?: string;
  title?: string;
  url?: string;
}

//<picture className="stories-carousel__media-image">
//  <source media="screen" src={props.src} />
//</picture>

export default (props: IStoriesCarouselItem) => {
  if (!props.src) {
    return <CarouselItem itemClass="stories-carousel__item" />;
  }
  return (
    <CarouselItem itemClass="stories-carousel__item">
      <img className="stories-carousel__media-image" src={props.src} alt={props.title} />
    </CarouselItem>
  );
};
