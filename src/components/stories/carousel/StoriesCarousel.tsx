import * as React from 'react';

import Carousel from 'src/components/carousel/Carousel';
import StoriesCarouselItem, { IStoriesCarouselItem } from './StoriesCarouselItem';

interface IStoriesCarouselProps {
  items: IStoriesCarouselItem[];
}

export default class StoriesCarousel extends React.Component<IStoriesCarouselProps, {}> {
  public render() {
    return (
      <div className="stories-carousel">
        <Carousel carouselClass="stories-carousel__carousel" items={this.props.items}>
          {this.props.items.map(itemProps => (
            <StoriesCarouselItem key={itemProps.key} {...itemProps} />
          ))}
          <StoriesCarouselItem key="last" />
        </Carousel>
      </div>
    );
  }
}
