import * as React from 'react';

export default props => {
  return <li className={`${props.itemClass || ''} lr-carousel__slide js_slide`}>{props.children}</li>;
};
