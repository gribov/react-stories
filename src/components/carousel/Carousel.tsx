import * as React from 'react';
import { lory } from 'lory.js';

interface ICarouselProps {
  items: any[];
  carouselClass?: string;
  children: any;
}

export default class Carousel extends React.Component<ICarouselProps, {}> {
  private readonly carouselRef: React.RefObject<HTMLDivElement>;

  public constructor(props) {
    super(props);
    this.carouselRef = React.createRef<HTMLDivElement>();
  }

  public render() {
    return (
      <div className={`${this.props.carouselClass || ''} lr-carousel`} ref={this.carouselRef}>
        <div className="lr-carousel__frame js_frame">
          <ul className="lr-carousel__slides js_slides">{this.props.children}</ul>
        </div>
      </div>
    );
  }

  public componentDidMount(): void {
    // wait for image loading
    setTimeout(() => lory(this.carouselRef.current), 100);
  }

  public componentDidUpdate(prevProps: Readonly<ICarouselProps>): void {
    if (this.props.children !== prevProps.children) {
      lory(this.carouselRef.current);
    }
  }
}
