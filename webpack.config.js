const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

const DEV_MODE = process.env.NODE_ENV !== 'production';

module.exports = {
  mode: DEV_MODE ? 'development' : 'production',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'public', 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(sass|scss)$/,
        use: [
          DEV_MODE ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: 'ts-loader'
      },
      {
        test: /\.(ts|tsx)$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: [
          {
            loader: 'tslint-loader',
          }
        ]
      },
      // {
      //   test: /\.(js|jsx)$/,
      //   exclude: /node_modules/,
      //   loader: 'babel-loader',
      //   options: {
      //     presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-es2015']
      //   }
      // },

    ]
  },
  optimization: {
    minimizer: [
      new UglifyJSPlugin({
        sourceMap: DEV_MODE,
        uglifyOptions: {
          output: {
            comments: false
          }
        }
      })
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx','.css', '.sass', '.sсss'],
    alias: {
      'src': path.resolve(__dirname, './src/')
    },
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
    }),
    new OptimizeCssAssetsPlugin(),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(process.env.NODE_ENV)
    })
  ],
};

if (DEV_MODE) {
  module.exports.devtool = 'inline-source-map';
  module.exports.devServer = {
    historyApiFallback: true,
    port: 9000,
  };
  module.exports.plugins.push(new HtmlWebpackPlugin({
    template: 'public/index.html',
  }));
}